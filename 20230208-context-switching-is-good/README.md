# Context Switching is Good
**Subtext:** Don't let the distraction get in the way of opportunity  
**Type:** Ignite  
**Time:** 5m  

## Develop

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='Context Switching is Good'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8000:8000 -p 35729:35729 \
    cloudogu/reveal.js:dev
```

## Present

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='Context Switching is Good'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

## Uses:
- **20230208:** Cloud Security Staff Meeting
  - **Audience:**
    - **Count:** 25
    - **Type:** Four delivery pods/teams with agile, product, and engineering titles
