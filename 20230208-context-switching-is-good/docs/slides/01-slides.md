<style>
.container{
  display: grid;
  grid-auto-flow: column;
}

.yellow-accent{
  color: #ffbf27;
}
</style>


## But I agree there are <font class=yellow-accent>bad</font> context switches


Notes:
- There is some validity, we've all seen it, felt it, tried to reduce it
- There _is_ a cost, we often hear 30min

---

## What _is_ a <font class=yellow-accent>Context Switch</font>

While working...

1. <font class=yellow-accent>interrupt!</font>
2. <font class=yellow-accent>save</font> thread state
3. <font class=yellow-accent>restore</font> thread state

...continue working

Notes:
- CPU processor architecture language, but applies to us

---

## How we work

<div class="container" >
  <div class="col" >

<img data-src="/images/counting-rice.jpg" width="100%" />
<img data-src="/images/miro.jpg" width="100%" />

  </div>
  <div class="col" style="text-align: left" >

<img data-src="/images/code.png" width="100%" />
<img data-src="/images/architecture.png" width="100%" />

  </div>
</div>

Notes:
- Which of these is not similar to the rest
- Captcha: pick the simpleton

---

## Meanwhile, our brain

<img data-src="/images/brain-saffolding.png" width="50%" />

Notes:
- It's not a serial processor
- [Study](https://www.frontiersin.org/articles/10.3389/fnhum.2014.00051/full) on the networking of human brain white matter and sensitivity to injury

---

## Hot Take

<i class="fas fa-fire"></i>

~~Fallacy of multitasking~~  
<font class=yellow-accent>Fallacy that all context swithches are bad</font>


Notes:
- Unless we're counting rice, who's mind truly does not deviate from the current task when _100%_ focused?
- We don't wany to stop interrupts all together, we utilize them for inspiration, problem solving
- Our mind is not serial
- It leverages a lattice structure to solve complex problems

---

## Distraction vs <font class=yellow-accent>Opportunity</font>?

Ask this question every interrupt

(it's already happening subconciously)

Notes:
- I'd like to introduct some nuance

---

## Case Study

Developer comes across some bad code...

1. <font class=yellow-accent>Ignore</font> it
2. <font class=yellow-accent>Fix</font> it on the spot
3. <font class=yellow-accent>Share</font> it

Notes:
- If ignored, who will find the turd in future? Probably he/she who ignored.
- bad code == interruption

---

## Spotlight: Fixing On the Spot

- <font class=yellow-accent>Discernment</font> is a skill
- Experience helps
- Hold yourself accountable, be a professional

Note:
- "Quick aside, I've ignored #1, you should to, and #3 we'll talk about later"

---

## More Case Studies

- Teammate comes across a customer complaint...
- Teammate notices an inefficient process...
- Teammate reads something wrong in documentation...
- Teammate gets an email to do something manual and toilsome...
- Teammate reads the release notes for an awesome new AWS service that solves a needed problem...
- ...I could go on

<font class=yellow-accent>What do we do with these interrupts?</font>

Notes:
- What do we do with these interrupts? How many of them do you have in a day?

---

<h2>Capture the Opportunity <i class="fas fa-flag yellow-accent"></i></h2>


What to do with helpful interrupts:

  1. Ignore it
  2. Fix it on the spot
  3. ~~Share it~~ <font class=yellow-accent>Capture it</font>

---

## Why Does This Matter?

- Every day, significant amount of opportunity is <font class=yellow-accent>lost</font>
  - Incongruent with our continuous improvement mentality

Notes:
- We _think_ we'll remember, or someone else will handle it
- It doesn't have to be this way
- We'll also survive without capturing the opportunity...

---

## What to do about it?

<div class="container" >
  <div class="col" >

1. Ignore it------------->
2. Fix it on the spot--->
3. Capture it----------->

  </div>
  <div class="col" style="text-align: left" >

1. Discourage supression
2. Discern whether to fix
3. <font class=yellow-accent>Reduce the cost of capture</font>

  </div>
</div>


Notes:

---

## Measuring the Cost 

<font class=yellow-accent><h3>TIME</h3></font>

(Same goes for context switches as a whole)

Notes:
- We talk about 30min, CPU architects talk about the same
- It also translates to economics

---

<font class=yellow-accent><h2>MTTF, MTTC, & MTTR</h2></font>

- Mean Time to Fix vs Mean Time to Capture
  - MTTF >= MTTC : normal
  - MTTF < MTTC : abnormal
- Mean Time to Restore

Notes:

---

## Reducing <font class=yellow-accent>Capture Cost</font>

- Reduce inertia to raise visibility and make stories

Notes:

---

## #team-cloudsecurity-product

Attempting to reduce inertia for...

- <font class="yellow-accent">**Raising visiblity**</font>
  - Reacji channeler
  - Channel email address
  - Manual copy/paste
- <font class="yellow-accent">**Story creation**</font>
  - Product roles assisting story creation
  - (Future) Hackathon Jira story creator

Notes:
  - Reacji channeler: easy to "react" to a message and have it auto-posted in product channel
  - Channel email address: easy to forward opportunity (hint: save the ugly email as a contact!)
  - Simply copy pasta of a DM, private channel message, whatever: slightly more inertia but at least openly visible
  - Product roles assisting story creation: this is a balance, but I do request our Product roles help reduce MTTC
  - (Future) Hackathon Jira story creator: we built it, let's get it in a state where it's helping daily

---

## What our product channel is <font class=yellow-accent>NOT</font>

**The leader's (or anyone's) <font class=yellow-accent>short circuit</font> of prioritized work**

Notes:

---

## Summary

- Complex work leverages our brain's ability to stitch many contexts together and find solutions
- <font class=yellow-accent>Reduce distracting context switches</font> like an adult
- <font class=yellow-accent>Increase captured opportunity</font> from helpful context switches like a professional
- Leverage tooling to <font class=yellow-accent>reduce capture cost</font>
- Ensure you have suppoting backlog grooming discipline

Notes:

---

## Thank you!

Notes:

