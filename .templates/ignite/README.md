# TITLE
**Subtext:** SUBTEXT  
**Type:** Ignite  
**Time:** 5m

## Develop

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='TITLE'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8000:8000 -p 35729:35729 \
    cloudogu/reveal.js:dev
```

## Present

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='TITLE'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

## Export

_Ensure host port is consistent between commands_

#### Present with a clean output

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='TITLE'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='controls: false, progress: false, slideNumber: false' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

#### Export PDF

```
decktape reveal \
    --pdf-author 'John-Paul Herold' \
    --pdf-title 'TITLE' \
    'http://localhost:8080' \
    ~/path/to/title.pdf
```

## Uses:
- **YYYYMMDD:** EVENT NAME
  - **Audience:**
    - **Count:** ESTIMATE
    - **Type:** DEMOGRAPHICS
  - **Video Link(s)**: [YouTube](youtube)
