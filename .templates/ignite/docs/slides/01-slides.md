<style>
.container{
  display: grid;
  grid-auto-flow: column;
}

</style>


## Normal slide
- Today we're here to talk about <font style="color: #ffbf27">stuff</font>  

---

## Slide with columns
<div class="container" >
  <div class="col" >

- Left Column
- Cool content

  </div>
  <div class="col" style="text-align: left" >

- Right column
- Cooler content

  </div>
</div>

Notes:
- Awesome speaker notes

---

## Slide with text and picture
<div class="container" >
  <div class="col" >

- Left Column
- Cool content

  </div>
  <div class="col" >
    <img data-src="/images/herold-headshot-soccer.png" width="50%" />
  </div>
</div>

Notes:
- Awesome speaker notes

---

## Slide with font awesome icons

<i class="fas fa-award" style="color: #ffbf27"></i> Certifications  
<i class="fas fa-book" style="color: #ffbf27"></i> Books  
<i class="fas fa-address-card" style="color: #ffbf27"></i> Titles  
<i class="fas fa-sitemap" style="color: #ffbf27"></i> Orgs  
<i class="fas fa-download" style="color: #ffbf27"></i> Download

---

## Slide with big picture
<img data-src="/images/ghent.jpg" width="100%" />

