<style>
.container{
  display: grid;
  gap: 10px;
  grid-auto-flow: column;
}

.size20{
  font-size: 20pt
}

.highlight{
  color: #ffbf27
}

.red{
  color: #d5152e
}

</style>


<!-- Slide 01 -->
## Lots 'o substance
<img data-src="/images/emoji-sprites.png" class="r-stretch" />

Notes:
- speak to the objects, humans, around, life on earth
- Colorado earth that my Collarbone lost to while mountain biking


---
<!-- Slide 02 -->
## Association
<img data-src="/images/vitruvian-emojis.jpg" class="r-stretch" />

Notes:
- we associate ourselves to it all
- subconciously, everything
- our image of the world is thus material, substantial
  - "Humans on Earth were surrounded by objects, and the image of the world in their subconscious was thus material and substantial"
- we're rooted, comforted


---
<!-- Slide 03 -->
## Deep space contrast
<img data-src="/images/krazy-kerbal.png" class="r-stretch" />

Notes:
- Imagine yourself an Earth born astronaut, on a generational flight in deep space
- It's just the ship's env & all within
- Less substance, more space, inifinite scale


---
<!-- Slide 04 -->
## The Desubstantiation Effect
<img data-src="/images/this-is-fine-kerbal.jpg" class="r-stretch" />

Notes:
- Lack of substance made astronauts _crazy_...lonely, with a side of anxiety
- They were uprooted
- "Concept from Astronautic Psychology"
- "...in deep space, away from the Solar System, the stars were only distant points of light and the galaxy was nothing more than a luminous mist."


---
<!-- Slide 05 -->
## Remote work trigger

<div class="container">
  <div class="col" >

- I questioned my sanity ~June 2020
- Was I exhibiting the Desubstantiation effect?
- It made me explore ways of staying connected to my work, despite being separated from the company and people

  </div>
  <div class="col">
    <img data-src="/images/deaths-end.jpg" />
  </div>
</div>

Notes:
- I read all this in Cixin Liu's (TODO: pronunciation) Death's End, the final book of Three Body Problem series
  - (I hear Netflix is producing a show for it)
- I was probably fiddling with yaml, annoyed by Cloud Formation, and wondered what am I actually doing related to flying people?

---
<!-- Slide 06 -->
## Am I alone?

<div class="container">
  <div class="col" >

- I don't think so
- Working [remote] requires effort to stay connected
- Disengaged workforce
  - Not high performing
  - Not generative

  </div>
  <div class="col">
    <img data-src="/images/narcos.jpg" class="r-stretch" />
  </div>
</div>

Notes:
- ...and I bet there's a few of us who have struggled at times with this
- ...to company, your work, and the people you work with
- The Great Resignation
  - Is there less inertia to leaving?
- Overemployment


---
<!-- Slide 07 -->
## I <i class="fas fa-heart highlight"></i> Remote work
- This is not a rant against remote work
- This is recognizing that remote work _might_ trigger a pseudo Desubstantiation Effect for some of us
- A sense of isolation, anxiety, disconnect

Notes:
- ...in fact, I'd sing it's general praises in a different talk


---
<!-- Slide 08 -->
## Tips Please
- _Thrive_ in a distributed workforce
- Approach:
  - <i class="fas fa-building highlight"></i> Connection to company
  - <i class="fas fa-laptop highlight"></i> Your work/objective(s) relative to company
  - <i class="fas fa-user highlight"></i> The people you're working with

Notes:
- So let's beat it
- Here are some tips I found helpful, grouped like above, starting with people because I think the easiest to apply now


---
<!-- Slide 09 -->
## <i class="fas fa-user highlight"></i> Knowing people beyond the work

<div class="container">
  <div class="col" >

- Take interest in well being of others
- Build trust
- Two-way street

  </div>
  <div class="col">
    <img data-src="/images/5-dysfunctions.jpg" />
  </div>
  <div class="col">
    <img data-src="/images/employee-engagement.jpg" />
  </div>
</div>

Notes:
- Know people > build trust > set foundation for high performance


---
<!-- Slide 10 -->
## <i class="fas fa-user highlight"></i> Keep Things Fresh
- Alter your learning environment
  - Meeting methods
  - Teaching style
  - Collaboration techniques
- Gather feedback & iterate

Notes:
- ...avoid monotony
- When you _are_ in person...treat that type of work unique (hackathon)
  - don't go into the office and work the same as when remote (e.g. getting on video calls)
  - ...get out to a conference together (shout out)


---
<!-- Slide 11 -->
## <i class="fas fa-user highlight"></i> Less DMs, more channels <br>...and video

<div class="container">
  <div class="col" >

- There _is_ a purpose for DMs
- But, if working on team objectives, use a channel
  - Observation
  - Collaboration
  - Review
- Human expression > frozen avatars

  </div>
  <div class="col">
    <img data-src="/images/pragmatic-programmer.jpg" />
  </div>
</div>

Notes:
- DMs can be used for coaching, other private stuff
  - by not sharing a team objective related bit of info, you are incurring the additional work of repeating yourself many more times
- default to channels
  - ideally public


---
<!-- Slide 12 -->
## <i class="fas fa-laptop highlight"></i> Product thinking

<div class="container">
  <div class="col" >

- Know your customer (and market)
- Identify underserved needs
- Build
- <font class="highlight">Measure value</font>
- Iterate

  </div>
  <div class="col">
    <img data-src="/images/lean-product-playbook.png" />
  </div>
</div>

Notes:
- This helps you connect your work to relevance...improving human lives
- ...if you know who your building something for and why, you'll be more connected
- Watch them use your product (user studies)

---
<!-- Slide 13 -->
## <i class="fas fa-laptop highlight"></i> Product Insights

<div class="container">
  <div class="col" >

- We want our work to have measurable relevance
- Explore measurement of value...
  - Is your product used?
  - How?
  - By whom?
  
  </div>
  <div class="col">

> The thing I have noticed is when the anecdotes and the data disagree, the anecdotes are usually right. <br> -- Jeff Bezos

  </div>
</div>

Notes:
- Last tip can be a little abstract on it's own
- Must have user level granularity


---
<!-- Slide 14 -->
## <i class="fas fa-building highlight"></i> Macro Connection
- Know your company's mission statement and values
- Learn about other departments and roles
- Read the history of your industry & company
- Track stock price, read quarterly financials, general news
- Follow on ~~Twitter~~ X, ring the <i class="fas fa-bell red"></i> on YouTube

Notes:
- Connect your team, your work, to your company
- Look beyond your team, your work
- See if you can help out for a day in the field
- Get to know these other people beyond their work 

---
<!-- Slide 15 -->
## <i class="fas fa-building highlight"></i> Swag

<div class="container">
  <div class="col" >

- Remind yourself who you work for
- Wear it, display it, have fun with it

  </div>
  <div class="col">
    <img data-src="/images/swa-desk-plane.jpg" />
  </div>
</div>

Notes:
- stickers, clothing, art, anything
- have _something_ related to work visible
- RPi for FlightAware


---
<!-- Slide 16 -->
## <i class="fas fa-industry highlight"></i> Community
- Conferences, user groups, meetups
- Strengthens your network
- Invite your coworkers!
- Share the goods

Notes:
- Bonus category!

---
<!-- Slide 17 -->
## Leader Application

<div class="size20">

| <font class='highlight'>Tip</font> | <font class='highlight'>Informal Leadership</font> | <font class='highlight'>Leader</font> |
| --- | --- | --- |
| Know people| Take interest in teammates | Read Lencioni and practice  |
| Keep fresh | Meaningful retros | Seek inspiration and initiate |
| Comm | Quote reply in channel | Measure usage patterns and manage channels |
| Product thinking | Ask about the customer | Align all on delivery of value |

</div>

---
<!-- Slide 18 -->
## Leader Application

<div class="size20">

| <font class='highlight'>Tip</font> | <font class='highlight'>Informal Leadership</font> | <font class='highlight'>Leader</font> |
| --- | --- | --- |
| Product insights | Add "use" metrics to AC | Be a [power] user of product data |
| Macro | Follow company news | Invite guests to staff meetings |
| Swag | Seek out cool stuff | Onboarding welcome package |
| Community | More DevOpsDays | Fund and request they share back or present to give back |

</div>


---
<!-- Slide 19 -->
## Thank you!

<div class="container" >
  <div class="col" >

- Connect to your company, relate your work, and build great relations with those you work with!
- Apply these tips to any work environment and thwart the Desubstantiation Effect!
- devopsdays@jph.slmail.me

  </div>
  <div class="col">
    <img data-src="/images/herold-headshot-soccer.png" />
    <img data-src="/images/cloud-security-lock.png" />
  </div>
  <div class="col">
    <img data-src="/images/swa.png" />
    <img data-src="/images/presentation-qr.png" />
  </div>
</div>

Notes:
- Happy to continue talking about any of this in Open Space
- Attendee, volunteer, organizer since 2017
