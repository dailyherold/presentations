# Remote Work & The Desubstantiation Effect
**Subtext:** Increase Engagement From Anywhere  
**Type:** Ignite  
**Time:** 5m

## Develop

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='Remote Work & The Desubstantiation Effect'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8000:8000 -p 35729:35729 \
    cloudogu/reveal.js:dev
```

## Present

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='Remote Work & The Desubstantiation Effect'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

## Export

_Ensure host port is consistent between commands_

#### Present with a clean output

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='Remote Work & The Desubstantiation Effect'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='controls: false, progress: false, slideNumber: false' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

#### Export PDF

```
decktape reveal \
    --pdf-author 'John-Paul Herold' \
    --pdf-title 'Remote Work & The Desubstantiation Effect' \
    'http://localhost:8080' \
    ~/path/to/remote-work-desubstantiation.pdf
```

## Uses:
- **20230830:** DevOpsDays DFW
  - **Audience:**
    - **Count:** 450 + livestream
    - **Type:** Mixed
  - **Video Link(s)**: [YouTube](https://www.youtube.com/watch?v=kGHeHGSS8Gg)
