# dailyherold-presentations

Repository for codified public presentations used at conferences, meetups, and random gatherings. Powered by [reveal.js](https://revealjs.com).

### Why do this?
- Good for my own content creation process, often continuing ideas from my [Zettlekasten](https://zettelkasten.de/introduction/)
- Plaintext content and browser/html rendering allows version control, human readability, and the content can run on nearly any device
- Provides easy way to share with others (e.g. QR code embedded in final slide), which is especially helpful for mentioned references/bibliography

## Installation
- [Install Docker](https://docs.docker.com/engine/install/) or whatever you want to run OCI containers
- [Install DeckTape](https://github.com/astefanutti/decktape) for great PDF export support
- `git clone git@gitlab.com:dailyherold/presentations.git`

## Usage
- From project root, `cp -r .templates/ignite YYYYMMDD-name-of-presentation`
- `cd` into a presentation directory
- Update the `README.md` and `docs/slides/*.md` as needed
- Follow the `Development` or `Present` instructions to run the presentation using the handy [cloudogu/reveal.js](https://github.com/cloudogu/reveal.js-docker) Docker image
- Open browser and navigate to http://localhost:8000 if developing, or http://localhost:8080 if presenting
  - The container will fail to start if there is port conflict, get around this by binding an alternate free port on the host to the container's port, e.g. `-p 8085:8080`
- If developing, changes will be immediately visible thanks to live reloading!
- (Optional) export a PDF via DeckTape by following the `Export` instructions in the presentation's README

## Presentation video index

- 20230830: [Remote Work & The Desubstantiation Effect](https://www.youtube.com/watch?v=kGHeHGSS8Gg)

## Support
[Open an issue](https://gitlab.com/dailyherold/presentations/-/issues) please!

## Roadmap
- [ ] Add DeckTape PDF export step to `.gitlab-ci.yml` for all `master` merges
- [ ] Convert to a static website with embedded viewing of presentations

## Contributing
I anticipate I'll be the only one contributing for now...but if the day comes, I'll add more details here.

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">Creative Commons Attribution-NoDerivatives 4.0 International License</a>.

