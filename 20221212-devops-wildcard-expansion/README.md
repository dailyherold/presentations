# \*Dev\*Ops\*
**Subtext:** Faster Time-to-Market Using Wildcard Expansion  
**Type:** Ignite  
**Time:** 5m  

## Develop

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='DevOps Wildcard Expansion'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8000:8000 -p 35729:35729 \
    cloudogu/reveal.js:dev
```

## Present

```
docker run --rm \
    -v $(pwd)/docs/slides:/reveal/docs/slides \
    -v $(pwd)/images:/reveal/images \
    -v $(pwd)/resources:/resources \
    -e TITLE='DevOps Wildcard Expansion'\
    -e THEME_CSS='moon.css' \
    -e ADDITIONAL_REVEAL_OPTIONS='autoSlide: 15000' \
    -p 8080:8080 \
    cloudogu/reveal.js
```

## Uses:
- **20221212:** Soar 2022 -- Southwest Airlines Technology Showcase
  - **Audience:**
    - **Count:** estimated 500 in-person, streamed live to more
    - **Type:** diverse given public event and SWA employees, but okay to lean technical since the event is a "Technology Showcase"
