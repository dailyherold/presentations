<style>
.container{
  display: grid;
  grid-auto-flow: column;
}
</style>


<div class="container" >
  <div class="col" >

## Greetings
- Sr Manager Cloud Security  
- ~4.5 years @ SWA  
- Joined for a required-to-fly cloud migration

  </div>
  <div class="col">
    <img data-src="/images/herold-headshot-soccer.png" width="100%" />
  </div>
</div>

Notes:
- Fortunate to have worked with everyone speaking today on that effort

---

## What about today?
- Today we're here to talk about <font style="color: #ffbf27">DevOps</font>  
- Right of passage after 8 years of being a self proclaimed practitioner  

Notes:
- Anyone heard of it?
- Read the books, held the title
- DevOpsDays attendee, volunteer, organizer  

---

## Overloaded DevOps

<i class="fas fa-award" style="color: #ffbf27"></i> Certifications  
<i class="fas fa-book" style="color: #ffbf27"></i> Books  
<i class="fas fa-address-card" style="color: #ffbf27"></i> Titles  
<i class="fas fa-sitemap" style="color: #ffbf27"></i> Orgs  
<i class="fas fa-download" style="color: #ffbf27"></i> Download

Note:
- What I've come to realize is that devops is overloaded

---

## All that isn't DevOps
- The ethos of a DevOps cultures is...
- <font style="color: #ffbf27">Delivering valuable, reliable, secure software faster than competition</font>
- Using technology to differentiate
- Good economics!

---

## Founding fathers
- Patrick Debouis and Andrew Clay Schafer realized this  
- <font style="color: #ffbf27">Dev wanted</font> wanted inovation and <font style="color: #ffbf27">change</font>  
- <font style="color: #ffbf27">Ops wanted</font> stability and <font style="color: #ffbf27">no change</font>  
- The alignment between the two silos of Dev and Ops was delivery  

Note:
- Also if Ops devs more, and Dev ops more...good things happen too

---

## Overloaded Bandwagon
- Since the DevOps explosion of 2009 we see all types of divergent trends  
- DevSecOps, ChatOps, FinOps, AIOps, NoOps, ConferenceOps...
- Any other \*Ops?

Notes:
- DevSecOps: welcome Cybersecurity to the party
- ChatOps: using chat tools to operate with a convenient UX
- FinOps: using Finance to operate...better? Nah, cloud financial management
- AIOps: Gartner trying to make more money
- NoOps: offload all operations to code or managed service
- ConferenceOps: writing code to help run a conference

---

## Time to realign...again
- All of the variations have confused, or distracted us from the point
- BUT, it's a confirmation that <font style="color: #ffbf27">DevOps has resonated beyond its organizational namesakes</font>

Notes:
- Our recruiting team's heads spin trying to understand latest trends
- Our heads spin trying to explain DevOps to people around us

---

## Delivery
- We seek delivery of differentiated value  
- To do that well involves a lot more people than just Dev and Ops  
- ...or the other dyads and triads mentioned  

Notes:
- Can you picture some of these other people, groups, possible constraints?

---

## Patrick @ Ghent
<img data-src="/images/ghent.jpg" width="100%" />

Notes:
- What groups in your companies are involved in holistic product delivery?

---

## Sub-optimization
<img data-src="/images/suboptimization.png" width="100%" />

---

## How do you know who is involved?
- If you've been around your company for longer than onboarding, you probably have some leads
- Practically, we need to walk the work through, and map it
- Gain buy in from your leader to allow you to trace something from start to finish, genesis to delivery

---

## Instagram vs Reality
<img data-src="/images/imagined-vs-done.png" width="100%" />

Notes:
- Which of you have fallen in the trap of thinking work like this? Me
- Is it shocking that we often don't have enough margin, or buffer, in our planning to account for unplanned work, and unexpected new information

---

<div class="container" >
  <div class="col" >

## High five Toyota
- Thanks to TPS we have something even better
  - ~~Material & Information Flow~~
  - Value stream mapping
- Rooted at the customer, framework with some structure
  - Product flow as well as information flow

  </div>
  <div class="col">
    <img data-src="/images/learning-to-see.png" width="100%" />
  </div>
</div>

---

## What's the <font style="color: #ffbf27">goal</font> though?
- Awareness
- Good discussions
- Future state
- Elimination of waste

---

## How do you take action?
- Put yourself in positions outside of your normal domain
- Learn new mental models, build a latticework like Charles Munger
- Influence for the sake of delivery

---

## I've had fun doing this
- I moved to Cybersecurity
- I kept my suboptimization blinders on too long

Notes:
- Aligned with all types of things I've been practicing and studying
- Thank you to my leaders, peers, and people for showing me grace in the transition

---

<h2 style="color: #ffbf27">*Dev*Ops*</h2>

- Whether security, finance, legal, HR, customer success, product, procurement, whatever
- Do some wildcard expansion and think about the holistic value stream
- Identify waste, constraints, opportunity
- Visualize, be positive, tell the truth, but align on the common goal and iterate

---

## DevOps culture leads to...
- <font style="color: #ffbf27">Delivering valuable, reliable, secure software faster than competition</font>
- Valuable = customer focus  
- Reliable = great service  
- Secure = responsibility  
- Faster than competition = good economics

---

## Enlist
- We all own delivery ([just like availability](https://www.whoownsmyavailability.com/))
- I challenge you to think wild...beyond just technology
- Build up others, break down silos
- Influence the full value stream

---

<div class="container" >
  <div class="col" >

## Thank you!
- I'll be at HH
- Stay in touch
- Reach out on [LinkedIn](https://www.linkedin.com/in/johnpaulherold) or [Twitter](https://twitter.com/dailyherold_)

  </div>
  <div class="col">
    <img data-src="/images/qr-presentation.png" width="100%" />
  </div>
</div>
